# Digital Ocean CLI Tool Docker image

```bash
alias doctl='docker run -i -e DIGITALOCEAN_ACCESS_TOKEN=${DIGITALOCEAN_ACCESS_TOKEN} vidimensional/doctl'
```
