#!/bin/bash -x

set -e

VERSIONS="v1.7.1 v1.7.2"
LATEST="v1.7.2"
REPO='vidimensional/doctl'

source ../scripts/functions.sh
_docker_build_push
