.PHONY: psql mysql-client jq

all: psql mysql-client jq

psql:
	@echo '==> Building psql Docker image.'
	docker build -t vidimensional/psql -f psql/Dockerfile psql/

mysql-client:
	@echo '==> Building mysql-client Docker image.'
	docker build -t vidimensional/mysql-client -f mysql-client/Dockerfile mysql-client/

jq:
	@echo '==> Building jq Docker image.'
	docker build -t vidimensional/jq -f jq/Dockerfile jq