#!/bin/bash -x

set -e

VERSIONS="1.5"
LATEST="1.5"
REPO='vidimensional/jq'

source ../scripts/functions.sh
_docker_build_push
