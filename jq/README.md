# jq Docker image

```bash
alias jq='docker run -i -v "\${PWD}:/opt/jq/workspace" vidimensional/jq'
```
