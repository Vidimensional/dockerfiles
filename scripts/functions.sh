#!/bin/bash
set -e

_docker_build_push() {
    for version in $VERSIONS; do
      #export VERSION=$version
      docker build \
      --tag "${REPO}:${version}-${TRAVIS_COMMIT}" \
      --build-arg "VERSION=$version" .
    done

  for version in $VERSIONS; do
    docker tag \
      "$REPO:${version}-${TRAVIS_COMMIT}" \
      "${REPO}:$version"

      docker push "${REPO}:${version}-${TRAVIS_COMMIT}"
      docker push "${REPO}:$version"

      if [ "$version" == "$LATEST" ]; then
        docker tag "${REPO}:$version" "${REPO}:latest"
        docker push "${REPO}:latest"
      fi

    done
}
