# Vidimensional Dockerfiles

Sometimes I'm too lazy to install tools on every computer I use (go to install, ensure that the dependencies are satisfied, checkout that hose dependencies doesn't collide with the ones of other tool, etc.). So I decided to package them inside a Docker container and have them available everywhere it has Docker (Linux and MacOS).